<?php

    namespace CmsTf\DevTools\Timer;

    use JsonSerializable;
    use CmsTf\DevTools\Timer\Exception\ReadOnlyException;

    /**
     * Class TimerManager
     *
     * @package CmsTf\DevTools\Timer
     * @author  Gerard Smit <gsmit1996@gmail.com>
     */
    class TimerManager implements TimerInterface, JsonSerializable {
        use TimerTrait;

        /**
         * @var null|string
         */
        protected $rootDir = null;

        /**
         * @var Timer[]
         */
        protected $timers = [];

        /**
         * @var Timer[]
         */
        protected $activeTimers = [];

        /**
         * @var int
         */
        protected $id = 0;

        /**
         * @var bool
         */
        protected $readOnly = false;

        /**
         * @var int
         */
        protected $depth = 0;

        /**
         * @return null|string
         */
        public function getRootDir() {
            return $this->rootDir;
        }

        /**
         * @param null|string $rootDir
         */
        public function setRootDir($rootDir) {
            $this->rootDir = $rootDir;
        }

        /**
         * Begin the time manager.
         */
        public function begin() {
            if ($this->readOnly) {
                throw new ReadOnlyException();
            }

            $this->setBeginTime(microtime(true));
            $this->setBeginMemory(memory_get_usage());
        }

        /**
         * End the time manager.
         */
        public function end() {
            if ($this->readOnly) {
                throw new ReadOnlyException();
            }

            while (count($this->activeTimers) > 0) {
                $this->stop();
            }

            $this->setEndTime(microtime(true));
            $this->setEndMemory(memory_get_usage());
            $this->readOnly = true;

            $this->sort();
        }

        /**
         * Sort the timers.
         */
        protected function sort() {
            usort($this->timers, function(Timer $a, Timer $b) {
                return ($a->getBeginTime() + $a->getDepth()) - ($b->getBeginTime() + $b->getDepth());
            });
        }

        /**
         * Get the current timer.
         *
         * @return null|Timer
         */
        public function getCurrent() {
            $count = count($this->activeTimers);

            if ($count === 0) {
                return null;
            }

            return $this->activeTimers[$count - 1];
        }

        /**
         * Start a new timer.
         *
         * @param string     $name   The name of the timer.
         * @param array      $params The parameters.
         * @param bool       $child  True if the timer is a child.
         * @param null|array $trace
         *
         * @return Timer
         */
        public function start($name, array $params = [], $child = false, $trace = null) {
            if ($this->readOnly) {
                throw new ReadOnlyException();
            }

            // Get the current timer.
            $current = $this->getCurrent();

            // Stop the previous timer if it's a child.
            if ($child && $current !== null && $current->isChild()) {
                $this->stopCurrent();

                $current = $this->getCurrent();
            }

            // Create the timer.
            $timer = new Timer($this);

            if ($child) {
                $timer->setId(count($current->getChildren()));
            } else {
                $timer->setId($this->id ++);
            }

            if ($trace === null) {
                $trace = debug_backtrace();
            }

            $timer->setName($name);
            $timer->setFile($this->rootDir !== null ? substr($trace[0]['file'], strlen($this->rootDir) +
                1) : $trace[0]['file']);
            $timer->setFileLine($trace[0]['line']);
            $timer->setParameters($params);
            $timer->setDepth($this->depth);
            $timer->setBeginTime(microtime(true));
            $timer->setBeginMemory(memory_get_usage());
            $timer->setChild($child);

            // If the timer is not a child, update the depth.
            if (!$child) {
                $this->depth ++;
            }

            // Add the timer to the active timers.
            $this->activeTimers[] = $timer;

            return $timer;
        }

        /**
         * Start a new child timer.
         *
         * @param string $name   The name of the timer.
         * @param array  $params The parameters.
         *
         * @return void
         */
        public function child($name, array $params = []) {
            $this->start($name, $params, true, debug_backtrace());
        }

        /**
         * Stop the current timer.
         *
         * @param array $params The parameters.
         *
         * @return null|Timer
         */
        protected function stopCurrent($params = []) {
            /** @var Timer $timer */
            $timer = array_pop($this->activeTimers);

            if ($timer === null) {
                return null;
            }

            // Append the duration.
            $timer->setEndMemory(memory_get_usage());
            $timer->setEndTime(microtime(true));
            $timer->setParameters(array_replace($timer->getParameters(), $params));

            $current = $this->getCurrent();

            if (!$timer->isChild()) {
                $timer->setParent($this);
                $this->timers[] = $timer;
                $this->depth --;
            } else {
                $timer->setParent($current);
                $current->addChild($timer);
            }

            return $timer;
        }

        /**
         * Stop the current timer.
         *
         * @param array $params The parameters.
         *
         * @return void
         */
        public function stop($params = []) {
            $current = $this->getCurrent();

            if ($current !== null && $current->isChild()) {
                $this->stopCurrent();
            }

            $this->stopCurrent($params);
        }

        /**
         * @return Timer[]
         */
        public function getTimers() {
            return $this->timers;
        }

        /**
         * @return Timer[]
         */
        public function getActiveTimers() {
            return $this->activeTimers;
        }

        /**
         * @return bool
         */
        public function isReadOnly() {
            return $this->readOnly;
        }

        /**
         * Get the max depth.
         *
         * @return int
         */
        public function getMaxDepth() {
            $maxDepth = 0;

            foreach($this->timers as $timer) {
                if($timer->getDepth() > $maxDepth) {
                    $maxDepth = $timer->getDepth();
                }
            }

            return $maxDepth;
        }

        /**
         * Specify data which should be serialized to JSON
         * @link  http://php.net/manual/en/jsonserializable.jsonserialize.php
         * @return mixed data which can be serialized by <b>json_encode</b>,
         * which is a value of any type other than a resource.
         * @since 5.4.0
         */
        function jsonSerialize() {
            return [
                'depth' => $this->getMaxDepth(),
                'duration' => $this->getDuration(),
                'memory' => $this->getMemoryUsage(),
                'peak_memory' => memory_get_peak_usage(true),
                'timers' => $this->timers
            ];
        }
    }