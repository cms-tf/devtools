<?php

    namespace CmsTf\DevTools\Timer;

    /**
     * Class TimeTrait
     *
     * Implementation of the interface TimerInterface.
     *
     * @see TimerInterface
     *
     * @package CmsTf\DevTools\Timer
     * @author  Gerard Smit <gsmit1996@gmail.com>
     */
    trait TimerTrait {

        /**
         * @var float
         */
        protected $beginTime;

        /**
         * @var float
         */
        protected $endTime;

        /**
         * @var int
         */
        protected $beginMemory;

        /**
         * @var int
         */
        protected $endMemory;

        /**
         * @return float
         */
        public function getBeginTime() {
            return $this->beginTime;
        }

        /**
         * @param float $beginTime
         */
        public function setBeginTime($beginTime) {
            $this->beginTime = $beginTime;
        }

        /**
         * @return float
         */
        public function getEndTime() {
            return $this->endTime;
        }

        /**
         * @param float $endTime
         */
        public function setEndTime($endTime) {
            $this->endTime = $endTime;
        }

        /**
         * @return float
         */
        public function getDuration() {
            return $this->endTime - $this->beginTime;
        }

        /**
         * @return int
         */
        public function getBeginMemory() {
            return $this->beginMemory;
        }

        /**
         * @param int $beginMemory
         */
        public function setBeginMemory($beginMemory) {
            $this->beginMemory = $beginMemory;
        }

        /**
         * @return int
         */
        public function getEndMemory() {
            return $this->endMemory;
        }

        /**
         * @param int $endMemory
         */
        public function setEndMemory($endMemory) {
            $this->endMemory = $endMemory;
        }

        /**
         * @return int
         */
        public function getMemoryUsage() {
            return $this->endMemory - $this->beginMemory;
        }
    }