<?php

    namespace CmsTf\DevTools\Timer;
    use JsonSerializable;

    /**
     * Class Timer
     *
     * @package CmsTf\DevTools\Timer
     * @author  Gerard Smit <gsmit1996@gmail.com>
     */
    class Timer implements TimerInterface, JsonSerializable {
        use TimerTrait;

        /**
         * @var int
         */
        protected $id;

        /**
         * @var string
         */
        protected $name;

        /**
         * @var string
         */
        protected $file;

        /**
         * @var int
         */
        protected $fileLine;

        /**
         * @var array
         */
        protected $parameters;

        /**
         * @var int
         */
        protected $depth;

        /**
         * @var bool
         */
        protected $child;

        /**
         * @var TimerManager
         */
        protected $timerManager;

        /**
         * @var Timer[]
         */
        protected $children = [];

        /**
         * @var TimerInterface
         */
        protected $parent = null;

        /**
         * Timer constructor.
         *
         * @param TimerManager $timerManager
         */
        public function __construct(TimerManager $timerManager) {
            $this->timerManager = $timerManager;
        }

        /**
         * @return int
         */
        public function getId() {
            return $this->id;
        }

        /**
         * @param int $id
         */
        public function setId($id) {
            $this->id = $id;
        }

        /**
         * @return string
         */
        public function getName() {
            return $this->name;
        }

        /**
         * @param string $name
         */
        public function setName($name) {
            $this->name = $name;
        }

        /**
         * @return string
         */
        public function getFile() {
            return $this->file;
        }

        /**
         * @param string $file
         */
        public function setFile($file) {
            $this->file = $file;
        }

        /**
         * @return int
         */
        public function getFileLine() {
            return $this->fileLine;
        }

        /**
         * @param int $fileLine
         */
        public function setFileLine($fileLine) {
            $this->fileLine = $fileLine;
        }

        /**
         * @return array
         */
        public function getParameters() {
            return $this->parameters;
        }

        /**
         * @param array $parameters
         */
        public function setParameters(array $parameters) {
            $this->parameters = $parameters;
        }

        /**
         * @return int
         */
        public function getDepth() {
            return $this->depth;
        }

        /**
         * @param int $depth
         */
        public function setDepth($depth) {
            $this->depth = $depth;
        }

        /**
         * @return bool
         */
        public function isChild() {
            return $this->child;
        }

        /**
         * @param bool $child
         */
        public function setChild($child) {
            $this->child = $child;
        }

        /**
         * @return Timer[]
         */
        public function getChildren() {
            return $this->children;
        }

        /**
         * @param Timer[] $children
         */
        public function setChildren(array $children) {
            $this->children = $children;
        }

        /**
         * Add a new child.
         *
         * @param Timer $timer
         */
        public function addChild(Timer $timer) {
            $this->children[] = $timer;
        }

        /**
         * @return TimerInterface|null
         */
        public function getParent() {
            return $this->parent;
        }

        /**
         * @param TimerInterface|null $parent
         */
        public function setParent($parent) {
            $this->parent = $parent;
        }

        /**
         * Specify data which should be serialized to JSON
         * @link  http://php.net/manual/en/jsonserializable.jsonserialize.php
         * @return mixed data which can be serialized by <b>json_encode</b>,
         * which is a value of any type other than a resource.
         * @since 5.4.0
         */
        function jsonSerialize() {
            $data = [
                'id' => $this->getId(),
                'name' => $this->getName(),
                'file' => $this->getFile(),
                'file_line' => $this->getFileLine(),
                'duration' => $this->getDuration(),
                'memory' => $this->getMemoryUsage(),
                'parameters' => $this->getParameters(),
                'child' => $this->isChild()
            ];

            if (!$this->child) {
                $data['depth'] = $this->getDepth();
                $data['children'] = $this->getChildren();
            }

            return $data;
        }
    }