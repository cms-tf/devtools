<?php

    namespace CmsTf\DevTools\Timer;

    /**
     * Interface TimeInterface
     *
     * @package CmsTf\DevTools\Timer
     * @author  Gerard Smit <gsmit1996@gmail.com>
     */
    interface TimerInterface {
        /**
         * Set the timestamp before the timer was started.
         *
         * @param float $beginTime The timestamp.
         *
         * @return float
         */
        public function setBeginTime($beginTime);

        /**
         * Get the timestamp before the timer was started.
         *
         * @return float
         */
        public function getBeginTime();

        /**
         * Set the timestamp after the timer was finished.
         *
         * @param float $endTime The timestamp.
         *
         * @return float
         */
        public function setEndTime($endTime);

        /**
         * Get the timestamp after the timer was finished.
         *
         * @return float
         */
        public function getEndTime();

        /**
         * Get the duration of the timer.
         *
         * @return float
         */
        public function getDuration();

        /**
         * Set the memory before the timer was started.
         *
         * @param int $beginMemory
         *
         * @return void
         */
        public function setBeginMemory($beginMemory);

        /**
         * Get the memory before the timer was started.
         *
         * @return int
         */
        public function getBeginMemory();

        /**
         * Set the memory after the timer was finished.
         *
         * @param int $beginMemory
         *
         * @return void
         */
        public function setEndMemory($beginMemory);

        /**
         * Get the memory after the timer was finished.
         *
         * @return int
         */
        public function getEndMemory();

        /**
         * Get the memory usage in bytes.
         *
         * @return int
         */
        public function getMemoryUsage();
    }