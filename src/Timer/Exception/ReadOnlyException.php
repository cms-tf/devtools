<?php

    namespace CmsTf\DevTools\Timer\Exception;

    use RuntimeException;

    /**
     * Class ReadonlyException
     *
     * @package CmsTf\DevTools\Timer\Exception
     * @author  Gerard Smit <gsmit1996@gmail.com>
     */
    class ReadOnlyException extends RuntimeException {
        /**
         * ReadOnlyException constructor.
         *
         * @param string         $message
         * @param int            $code
         * @param \Exception|null $previous
         */
        public function __construct($message = 'Cannot change the manger in readonly mode.', $code = 0, \Exception $previous = null) {
            parent::__construct($message, $code, $previous);
        }

    }