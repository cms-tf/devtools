<?php

    namespace CmsTf\DevTools;

    use Interop\Http\ServerMiddleware\DelegateInterface;
    use Interop\Http\ServerMiddleware\MiddlewareInterface;
    use Psr\Http\Message\ResponseInterface;
    use Psr\Http\Message\ServerRequestInterface;
    use CmsTf\DevTools\Timer\TimerManager;

    /**
     * Class DevToolsMiddleware
     *
     * @package CmsTf\DevTools
     * @author  Gerard Smit <gsmit1996@gmail.com>
     */
    class DevToolsMiddleware implements MiddlewareInterface {
        /**
         * The version of the middleware.
         */
        const VERSION = '1.0.0';

        /**
         * @var array
         */
        protected $keys = [];

        /**
         * @var TimerManager|null
         */
        protected $timerManager = null;

        /**
         * @return array
         */
        public function getKeys() {
            return $this->keys;
        }

        /**
         * @param array $keys
         *
         * @return $this
         */
        public function setKeys($keys) {
            $this->keys = $keys;

            return $this;
        }

        /**
         * @return null|TimerManager
         */
        public function getTimerManager() {
            return $this->timerManager;
        }

        /**
         * @param null|TimerManager $timerManager
         *
         * @return $this
         */
        public function setTimerManager($timerManager) {
            $this->timerManager = $timerManager;

            return $this;
        }

        /**
         * Process an incoming server request and return a response, optionally delegating
         * to the next middleware component to create the response.
         *
         * @param ServerRequestInterface $request
         * @param DelegateInterface      $delegate
         *
         * @return ResponseInterface
         */
        public function process(ServerRequestInterface $request, DelegateInterface $delegate) {
            $response = $delegate->process($request);

            if ($response !== null && $request->hasHeader('tf-auth')) {
                $key = $request->getHeader('tf-auth');

                if (!in_array($key, $this->keys, true)) {
                    $data = ['authorized' => false];
                } else {
                    $data = [
                        'authorized' => true,
                        'version' => self::VERSION,
                        'timer_manager' => $this->timerManager
                    ];
                }

                $response = $response->withHeader('TF-Extension', json_encode($data));
            }

            return $response;
        }
    }