<?php

    namespace CmsTf\DevTools\Tests;

    use PHPUnit\Framework\TestCase;
    use CmsTf\DevTools\Timer\Exception\ReadOnlyException;
    use CmsTf\DevTools\Timer\TimerManager;

    /**
     * Class TimerManagerTest
     *
     * @package CmsTf\DevTools\Tests
     * @author  Gerard Smit <gsmit1996@gmail.com>
     */
    class TimerManagerTest extends TestCase {
        /**
         * Test the flow of the timer manager.
         */
        public function testFlow() {
            $timerManager = new TimerManager();

            // Begin the manager.
            $timerManager->begin();
            self::assertFalse($timerManager->isReadOnly());
            self::assertCount(0, $timerManager->getActiveTimers());
            self::assertCount(0, $timerManager->getTimers());

            // Start a new timer.
            $timerManager->start('test');
            self::assertCount(1, $timerManager->getActiveTimers());

            // End the timer.
            $timerManager->stop();
            self::assertCount(0, $timerManager->getActiveTimers());
            self::assertCount(1, $timerManager->getTimers());

            // Start a new timer that with a child.
            $timer = $timerManager->start('test');
            $timerManager->child('child_1');
            $timerManager->child('child_2');
            $timerManager->stop();

            // The timer should have 2 children.
            self::assertCount(2, $timer->getChildren());

            // The manager should have 2 timers.
            self::assertCount(2, $timerManager->getTimers());

            // Start a new timer without stopping it, this should be handled in the end.
            $timerManager->start('test');

            // End the manager.
            $timerManager->end();
            self::assertCount(0, $timerManager->getActiveTimers());
            self::assertTrue($timerManager->isReadOnly());

            // When the manager is ended, it should locks itself.
            self::expectException(ReadOnlyException::class);
            $timerManager->start('test');
        }
    }